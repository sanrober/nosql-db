# Sanjay-Robertss-MacBook-Air:Desktop sanjayroberts1$ /usr/local/mysql/bin/mysql --local-infile=1 -u root -p

import numpy as np

rec = []
rec.append('red pizza')
rec.append('green pizza')
rec.append('blue pizza')
rec.append('yellow pizza')
rec.append('orange pizza')
rec.append('purple pizza')
rec.append('grey pizza')
rec.append('brown pizza')
rec.append('gold pizza')
rec.append('silver pizza')
rec.append('pink pizza')
rec.append('moss pizza')
rec.append('perwinkle pizza')
rec.append('pumpkin pizza')
rec.append('black pizza')
rec.append('citron pizza')
rec.append('coffee pizza')
rec.append('chocolate pizza')
rec.append('olive pizza')
rec.append('peach pizza')
rec.append('burnt umber pizza')
rec.append('salmon pizza')
rec.append('teal pizza')
rec.append('vermilion pizza')
rec.append('white pizza')

description = []
description.append('A delicious blend of herbs and spices layered on our crusty red pizza')
description.append('A delicious blend of herbs and spices layered on our crusty green pizza')
description.append('A delicious blend of herbs and spices layered on our crusty blue pizza')
description.append('A delicious blend of herbs and spices layered on our crusty yellow pizza')
description.append('A delicious blend of herbs and spices layered on our crusty orange pizza')
description.append('A delicious blend of herbs and spices layered on our crusty purple pizza')
description.append('A delicious blend of herbs and spices layered on our crusty grey pizza')
description.append('A delicious blend of herbs and spices layered on our crusty brown pizza')
description.append('A delicious blend of herbs and spices layered on our crusty gold pizza')
description.append('A delicious blend of herbs and spices layered on our crusty silver pizza')
description.append('A delicious blend of herbs and spices layered on our crusty pink pizza')
description.append('A delicious blend of herbs and spices layered on our crusty moss pizza')
description.append('A delicious blend of herbs and spices layered on our crusty perwinkle pizza')
description.append('A delicious blend of herbs and spices layered on our crusty pumpkin pizza')
description.append('A delicious blend of herbs and spices layered on our crusty black pizza')
description.append('A delicious blend of herbs and spices layered on our crusty citron pizza')
description.append('A delicious blend of herbs and spices layered on our crusty coffee pizza')
description.append('A delicious blend of herbs and spices layered on our crusty chocolate pizza')
description.append('A delicious blend of herbs and spices layered on our crusty olive pizza')
description.append('A delicious blend of herbs and spices layered on our crusty peach pizza')
description.append('A delicious blend of herbs and spices layered on our crusty burnt umber pizza')
description.append('A delicious blend of herbs and spices layered on our crusty salmon pizza')
description.append('A delicious blend of herbs and spices layered on our crusty teal pizza')
description.append('A delicious blend of herbs and spices layered on our crusty vermilion pizza')
description.append('A delicious blend of herbs and spices layered on our crusty white pizza')

instructions = []
instructions.append('Take the red dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the green dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the blue dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the yellow dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the orange dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the purple dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the grey dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the brown dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the gold dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the silver dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the pink dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the moss dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the perwinkle dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the pumpkin dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the black dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the citron dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the coffee dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the chocolate dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the olive dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the peach dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the burnt umber dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the salmon dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the teal dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the vermilion dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')
instructions.append('Take the white dough. Add a sprinkle of salt. Chop the veggies. Throw the cheese. Bake at 350 degrees for 25 minutes. Take out of oven to cool. Serve to customer.')

state = []
state.append('MO')
state.append('KS')
state.append('CO')
state.append('CA')
state.append('NY')
state.append('IL')
state.append('FL')
state.append('NM')
state.append('TX')
state.append('SD')
state.append('OR')
state.append('WY')

city = []
city.append('Denver')
city.append('Springfield')
city.append('Boulder')
city.append('Coaldale')
city.append('Jefferson')
city.append('Topeka')
city.append('Vail')
city.append('Breckenridge')
city.append('Santa fe')
city.append('Taos')
city.append('Trinidad')
city.append('Pueblo')

ingreds = []
ingreds.append('cheese')
ingreds.append('dough')
ingreds.append('pepper')
ingreds.append('parmesan')
ingreds.append('pepperoni')
ingreds.append('stuffed crust')
ingreds.append('anchovies')
ingreds.append('bacon')
ingreds.append('artichoke')
ingreds.append('mushroom')
ingreds.append('onion')
ingreds.append('red sauce')
ingreds.append('tomato')
ingreds.append('cilantro')
ingreds.append('pineapple')
ingreds.append('ham')
ingreds.append('spinach')
ingreds.append('potato')
ingreds.append('olive')
ingreds.append('peach')
ingreds.append('salt')
ingreds.append('salmon')
ingreds.append('pepper')
ingreds.append('flour')
ingreds.append('egg')


ingreds_desc = []
ingreds_desc.append('yummy cheese locally sourced from the farm')
ingreds_desc.append('yummy dough locally sourced from the farm')
ingreds_desc.append('yummy pepper locally sourced from the farm')
ingreds_desc.append('yummy parmesan locally sourced from the farm')
ingreds_desc.append('yummy pepperoni locally sourced from the farm')
ingreds_desc.append('yummy stuffed crust locally sourced from the farm')
ingreds_desc.append('yummy anchovies locally sourced from the farm')
ingreds_desc.append('yummy bacon locally sourced from the farm')
ingreds_desc.append('yummy artichoke locally sourced from the farm')
ingreds_desc.append('yummy mushroom locally sourced from the farm')
ingreds_desc.append('yummy onion locally sourced from the farm')
ingreds_desc.append('yummy red sauce locally sourced from the farm')
ingreds_desc.append('yummy tomato locally sourced from the farm')
ingreds_desc.append('yummy cilantro locally sourced from the farm')
ingreds_desc.append('yummy pineapple locally sourced from the farm')
ingreds_desc.append('yummy ham locally sourced from the farm')
ingreds_desc.append('yummy spinach locally sourced from the farm')
ingreds_desc.append('yummy potato locally sourced from the farm')
ingreds_desc.append('yummy olive locally sourced from the farm')
ingreds_desc.append('yummy peach locally sourced from the farm')
ingreds_desc.append('yummy salt locally sourced from the farm')
ingreds_desc.append('yummy salmon locally sourced from the farm')
ingreds_desc.append('yummy pepper locally sourced from the farm')
ingreds_desc.append('yummy flour locally sourced from the farm')
ingreds_desc.append('yummy egg locally sourced from the farm')


##############################

# db.users.insert(
#     { _id: "user_1", email_address:"Bob0@gmail.com",first_name: "Bob0", last_name:"Phillips0",phone: "12345",
#       address_line_1:"SuperCoolStreet0", address_line_2:"Apartment0",
#       city: "Denver", state:"CO", zip_code: "76545" }
# )

numUsers = 2000
outfile = open("tempUsers.js", "w")
for i in range(0, numUsers):
    outString = ''
    outString += 'db.users.insert({ _id: "user_'
    outString += str(i)  # _id
    outString += '", email_address:"Bob'
    outString += str(i)
    outString += '@gmail.com", first_name: "Bob' #email_address
    outString += str(i)  # first_name
    outString += '", last_name:"Phillips'
    outString += str(i)  #last_name
    outString += '",phone:"'
    outString += str(np.random.randint(1000000, 9999999))  # phone_number
    outString += '", address_line_1:"SuperCoolStreet'
    outString += str(i)  # address_line_1
    outString += '", address_line_2:"Apartment'
    outString += str(i)  # address_line_2
    outString += '", city: "'
    rNum1 = np.random.randint(12)
    outString += city[rNum1]  # city
    outString += '",state:"'
    rNum2 = np.random.randint(12)
    outString += state[rNum2]  # state
    outString += '", zip_code:"'
    zipcode = str(np.random.randint(1000000, 99999999))
    outString += zipcode  # zip_code
    outString += '"})'
    outString += "\n"
    outfile.write(outString)
outfile.close()


#############

#db.inventory.insert({ _id: "inv_1", name:"cheese",description: "yummy cheese",quantity:23})

numIngredients = 25
outfile = open("tempIngredients.js", "w")
for i in range(0, numIngredients):
    outString = ''
    outString += 'db.inventory.insert({ _id: "inv_'
    outString += str(i)  # inventory_id
    outString += '", name:"'
    outString += ingreds[i]  # name
    outString += '", description:"'
    outString += ingreds_desc[i]  # description
    outString += '",quantity:'
    quantity = str(np.random.randint(1000000, 9999999))
    outString += quantity # quantity
    outString += '})'
    outString += "\n"
    outfile.write(outString)
outfile.close()

######################
# db.recipes.insert(
#     { _id: "rec_1", name:"red pizza",description: "yummy red pizza",
#     ingredients:[{inventory_id:"inv_1", quantity:2},{inventory_id:"inv_4", quantity:6},{inventory_id:"inv_3", quantity:7}],
#     cooking_instructions:'take the dough'})


numRecipes = 25
outfile = open("tempRecipes.js", "w")
count=-3
for i in range(0, numRecipes):
    count+=3
    for j in range(0,1):
        outString = ''
        outString += 'db.recipes.insert({ _id: "rec_'
        outString += str(i)  # recipe_id
        outString += '", name:"'
        outString += rec[i]  # name
        outString += '", description: "'
        outString += description[i]  #description
        outString += '",ingredients:[{inventory_id:"inv_'
        outString += str((j + count) % 25)  # inventory_id
        outString += '", quantity:'
        quantity = str(np.random.randint(10, 50))
        outString += quantity # inv_quantity
        outString += '},{inventory_id:"inv_'
        outString += str((j + 1 + count) % 25)  # inventory_id
        outString += '", quantity:'
        quantity = str(np.random.randint(10, 50))
        outString += quantity # inv_quantity
        outString += '},{inventory_id:"inv_'
        outString += str((j + 2 + count) % 25)  # inventory_id
        outString += '", quantity:'
        quantity = str(np.random.randint(10, 50))
        outString += quantity # inv_quantity
        outString += '}],cooking_instructions:"'
        outString += instructions[i]  #cooking_instructions
        outString += '"})'
        outString += "\n"
        outfile.write(outString)
outfile.close()


############################
#db.orders.insert({ _id: "ord_1", user_id:"user_1",recipe_id:"rec_1", order_time: CurrentTime})
#add_order("ord_1", "user_1", "rec_1")


numOrders = 75000
outfile = open("tempOrders.js", "w")
for i in range(0, numOrders):
    outString = ''
    outString += 'add_order("ord_'
    outString += str(i)  # order_id
    outString += '","user_'
    user_id = str(np.random.randint(0, 2000))
    outString += user_id  # user_id
    outString += '","rec_'
    recipe_id = str(np.random.randint(0, 25))
    outString += recipe_id  # recipe_id
    outString += '")'
    outString += '\n'
    outfile.write(outString)
outfile.close()


































