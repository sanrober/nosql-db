SELECT *
FROM venues
WHERE street_address ILIKE '%blvd%';


/* My query is searching for all venues
   that have a street address that contains 'blvd'
   anywhere in the string. I used the % wildcards to
   match any strings that contain Blvd. Thus my pattern
   will match and return the following:
   2199 S University Blvd
   Hollywood blvd 123
   ThisisaBLVDdude
   */

