
//create database
mongo --host nosql-mongodb pizza
use pizza


//upload User data
load('/app/mongodb/examples/tempUsers.js')

//upload recipe data
load('/app/mongodb/examples/tempRecipes.js')


//upload inventory data
load('/app/mongodb/examples/tempIngredients.js')


//upload order data
load('/app/mongodb/examples/tempOrders.js')

