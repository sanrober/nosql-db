
Overall, it seemed like the project was designed for a graph database since we were dealing with relationships
between doctors, patients, illnesses, and treatments, where the actual relationship was just as important as
the nodes they connected. Thus it was easier and more fun to do the project in Neo4J than Postgres.

Creating the schema in Postgres was straight-forward since we were only creating entities with a name
(doctor 1, patient 1, treatment 1, illness 1, etc). I created a table for each, and then to represent the
many to many relationships between each, I had to build out a bunch of middle joining tables. On the other hand,
Neo4j made it much easier to represent many to many relationships (just add an arrow from a node!), thus actually
cutting down on the amount of work and data going into the database. Similarly, since doctors could also be
patients themselves, creating a dual label patient:doctor to link these two ideas in Neo4J was way easier and
intuitive. In Postgres doctors who were also patients were more or less treated as separate entities that had
the same name, but no specific link to ensure consistency.

Creating the Neo4J schema was easy as well since I could export the data from my RDBMS and import it into
Neo4J, just needing to specify nodes (each entity table in Postgres) and relationships (each joining table).
Neo4J made it very easy to do this, and also made cross-referencing my answers in the queryComparison
straightforward since I was using the same exact data in each database.

Querying for relationships in both databases was straightforward due to the schema. For Postgres, I only
needed to reference the joining tables to determine a patient’s doctor, illness, or treatment, and
showing all these in a combined statement of one query was as easy as inner joining all those tables.
Though I did notice with these queries that there was a bunch of duplicate data in the database i.e.
to represent relationships, I needed to list a patient’s name at least 4 times: once in the entity Patient table,
and then at least once in each of the join tables to create a relationship between a patient and doctor,
patient and illness, and patient and treatment. That same amount of overhead did not seem necessary in Neo4J.

For Neo4J, querying for data was made easy by Cypher’s intuitive syntax. Querying for a patient’s doctor,
illness, or treatment was as simple as finding all nodes with a specific label connected to the patient node
you were interested in, and doing the combined querying was just returning all nodes connected to a patient node.
Cypher syntax is SQL-esque so writing these queries was straightforward. Plus visualizing these in the UI
was a nice feature for Neo4J as well.

The extra credit questions for both were very similar in syntax. GroupBYs in Neo4J again were intuitive
and very similar to the structure in Postgres. Conceptualizing the relationships (i.e. which doctor is
treating the most unique illnesses, etc) was easier to think about in Neo4J, probably due to the graph structure/schema.

Lastly, I was surprised by the amount of online documentation and stackoverflow questions that existed for Neo4J.
It appears much more popular than I first imagined. Whenever I was stuck with either creating or querying the
database, it was nice to know that I could turn to the internet to find the help I needed.

Overall, I would say that Neo4J wins this time around. I thought it fit the task of ‘small town hospital’
quite well and represented the data intuitively and easily. Postgres was still up to the task, but it goes
to show why there has been such a rise in NoSQL databases  it was not the correct tool for the problem at hand.
Going forward, I will definitely consider the importance of representing relationships, and if that is a vital
feature to my data, then I will definitely consider Neo4J.

