# Sanjay-Houses-MacBook-Air:Desktop sanjayroberts1$ /usr/local/mysql/bin/mysql --local-infile=1 -u root -p

import numpy as np

numPatients = 10000
outfile = open("tempPatients.sql", "w")
outfile.write("INSERT INTO patients (patient_name) VALUES \n")
for i in range(0,35):
    outString = ""
    outString += "('Dr House"
    outString += str(i)  # doctors who are also patients
    outString += "'),"
    outString += "\n"
    outfile.write(outString)
for i in range(0, numPatients):
    outString = ""
    outString += "('Bob"
    outString += str(i)  # patient_name
    if i == 9999:
        outString += "');"
        outString += "\n"
    else: 
        outString += "'),"
        outString += "\n" 
    outfile.write(outString)
outfile.close()

##############################

numDoctors = 100
outfile = open("tempDoctors.sql", "w")
outfile.write("INSERT INTO doctors (doctor_name) VALUES \n")
for i in range(0, numDoctors):
    outString = ""
    outString += "('Dr House"
    outString += str(i)  # doctor_name
    if i == 99:
        outString += "');"
        outString += "\n"
    else: 
        outString += "'),"
        outString += "\n" 
    outfile.write(outString)
outfile.close()

########################################################

numIllnesses = 1000
outfile = open("tempIllnesses.sql", "w")
outfile.write("INSERT INTO illnesses (illness_name) VALUES \n")
for i in range(0, numIllnesses):
    outString = ""
    outString += "('Illness"
    outString += str(i)  # illness_name
    if i == 999:
        outString += "');"
        outString += "\n"
    else: 
        outString += "'),"
        outString += "\n"        
    outfile.write(outString)
outfile.close()

###############################################################

numTreatments = 750
outfile = open("tempTreatments.sql", "w")
outfile.write("INSERT INTO treatments (treatment_name) VALUES \n")
for i in range(0, numTreatments):
    outString = ""
    outString += "('Treatment"
    outString += str(i)  # illness_name
    if i == 749:
        outString += "');"
        outString += "\n"
    else:
        outString += "'),"
        outString += "\n"
    outfile.write(outString)
outfile.close()
 
##############################################################


numPatients = 10000
outfile = open("tempDocPat.sql", "w")
outfile.write("INSERT INTO doc_pat (doctor_name, patient_name) VALUES \n")
for i in range(0,35):
    num_docs = np.random.randint(2, 6)
    for j in range(1,num_docs):
        outString = ""
        outString += "('Dr House"
        rando_docs = np.random.randint(0,50)
        outString += str((i+j+rando_docs)%100)  # doctors for doctors who are also patients
        outString += "','Dr House"
        outString += str(i)  # doctors who are also patients
        outString += "'),"
        outString += "\n"
        outfile.write(outString)
for i in range(0, numPatients):
    num_docs = np.random.randint(2, 6)
    if i == 9999:
        num_docs = 2
    for j in range(1,num_docs):
        outString = ""
        outString += "('Dr House"
        rando_docs = np.random.randint(0,100)
        outString += str((i+j+rando_docs)%100)  # doctors for patients
        outString += "','Bob"
        outString += str(i)  # patient_name
        if i == 9999:
            outString += "');"
            outString += "\n"
        else: 
            outString += "'),"
            outString += "\n" 
        outfile.write(outString)
outfile.close()    

###############################################################

#create set of patients that have an illness to assign treatment later
patients_with_illness=set()

numPatients = 10000
outfile = open("tempIllPat.sql", "w")
outfile.write("INSERT INTO ill_pat (illness_name, patient_name) VALUES \n")
for i in range(0,35):
    num_ills = np.random.randint(0, 4)
    for j in range(0,num_ills):
        outString = ""
        outString += "('Illness"
        rando_ills = np.random.randint(0,1000)
        outString += str((i+j+rando_ills)%1000)  # illnesses that doctors have who are also patients
        outString += "','Dr House"
        outString += str(i)  # doctors who are also patients        
        outString += "'),"
        outString += "\n"
        outfile.write(outString)
        
        name = 'Dr House{}'.format(str(i))
        patients_with_illness.add(name)
        
        
for i in range(0, numPatients):
    num_ills = np.random.randint(0, 4)
    if i == 9999:
        num_ills = 1
    for j in range(0,num_ills):
        outString = ""
        outString += "('Illness"
        rando_ills = np.random.randint(0,1000)
        outString += str((i+j+rando_ills)%1000)  # illnesses that patients have
        outString += "','Bob"
        outString += str(i)  # patient_name
        if i == 9999:
            outString += "');"
            outString += "\n"
        else: 
            outString += "'),"
            outString += "\n" 
        outfile.write(outString)
        
        name = 'Bob{}'.format(str(i))
        patients_with_illness.add(name)
        
        
outfile.close()    

#print(patients_with_illness)
patients_with_illness = list(patients_with_illness)
print(len(patients_with_illness))


#################################################################


outfile = open("tempTreatPat.sql", "w")
outfile.write("INSERT INTO treat_pat (treatment_name, patient_name) VALUES \n")
final = patients_with_illness[-1]
for i in patients_with_illness:
    num_treats = np.random.randint(2, 6)
    if i == final:
        num_treats = 2
    for j in range(1,num_treats):
        outString = ""
        outString += "('Treatment"
        rando_treatments = np.random.randint(0,750)
        outString += str((j+rando_treatments)%750)  # treatments for patients
        outString += "','"
        outString += i  # patient_name
        if i == final:
            outString += "');"
            outString += "\n"
        else: 
            outString += "'),"
            outString += "\n" 
        outfile.write(outString)
outfile.close()    


###########################################################################

numPatients = 10000
outfile = open("tempDocPat.sql", "w")
outfile.write("INSERT INTO doc_pat (doctor_name, patient_name) VALUES \n")
for i in range(0,35):
    num_docs = np.random.randint(2, 6)
    for j in range(1,num_docs):
        outString = ""
        outString += "('Dr House"
        rando_docs = np.random.randint(0,50)
        outString += str((i+j+rando_docs)%100)  # doctors for doctors who are also patients
        outString += "','Dr House"
        outString += str(i)  # doctors who are also patients
        outString += "'),"
        outString += "\n"
        outfile.write(outString)
for i in range(0, numPatients):
    num_docs = np.random.randint(2, 6)
    if i == 9999:
        num_docs = 2
    for j in range(1,num_docs):
        outString = ""
        outString += "('Dr House"
        rando_docs = np.random.randint(0,100)
        outString += str((i+j+rando_docs)%100)  # doctors for patients
        outString += "','Bob"
        outString += str(i)  # patient_name
        if i == 9999:
            outString += "');"
            outString += "\n"
        else: 
            outString += "'),"
            outString += "\n" 
        outfile.write(outString)
outfile.close()    





























