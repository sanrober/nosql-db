/*  Create database */
createdb hospital
psql hospital



/*  Create patients table */
CREATE TABLE patients
(
    patient_name VARCHAR(255) PRIMARY KEY
);

/*  load script users table */
\i /app/postgres/lectures/tempPatients.sql


/*  Create doctors table */
CREATE TABLE doctors
(
    doctor_name VARCHAR(255) PRIMARY KEY
);

/*  load script users table */
\i /app/postgres/lectures/tempDoctors.sql


/*  Create illnesses table */
CREATE TABLE illnesses
(
    illness_name VARCHAR(255) PRIMARY KEY
);

/*  load script users table */
\i /app/postgres/lectures/tempIllnesses.sql


/*  Create treatments table */
CREATE TABLE treatments
(
    treatment_name VARCHAR(255) PRIMARY KEY
);

/*  load script users table */
\i /app/postgres/lectures/tempTreatments.sql


/*  Create doc_pat table */
CREATE TABLE doc_pat
(
    doctor_name  VARCHAR(255) REFERENCES doctors (doctor_name),
    patient_name VARCHAR(255) REFERENCES patients (patient_name)
);

/*  load script users table */
\i /app/postgres/lectures/tempDocPat.sql

/*  Create ill_pat table */
CREATE TABLE ill_pat
(
    illness_name VARCHAR(255) REFERENCES illnesses (illness_name),
    patient_name VARCHAR(255) REFERENCES patients (patient_name)
);

/*  load script users table */
\i /app/postgres/lectures/tempIllPat.sql


/*  Create treat_pat table */
CREATE TABLE treat_pat
(
    treatment_name VARCHAR(255) REFERENCES treatments (treatment_name),
    patient_name   VARCHAR(255) REFERENCES patients (patient_name)
);

/*  load script users table */
\i /app/postgres/lectures/tempTreatPat.sql










































