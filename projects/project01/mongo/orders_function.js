function add_order(order_id, user_id, rec_id){
    var order_time = ISODate();
    var still_true = true;

    db.recipes.find({"_id":rec_id},{"_id":0,"ingredients":1}).forEach(function(item){
        item.ingredients.forEach(function(order){
            var reduceBy = order.quantity * -1;
            var updInv = db.inventory.updateOne({_id: order.inventory_id,
                    quantity:{$gte:order.quantity}},
                {$inc: {quantity: reduceBy}});
            printjson(updInv.modifiedCount);
            if (updInv.modifiedCount == 0) {
                print('NO ORDER: Not enough ingredients');
                still_true = false;

            } else {
                print('Cool, enough ingredients');
            }

        })
    })

    if (still_true == true) {
        print('ORDER UP');
        db.orders.insert({
            _id: order_id,
            user_id: user_id,
            recipe_id: rec_id,
            order_time : order_time
        });
    } else {
        print('NO ORDER BRO'); }
}
