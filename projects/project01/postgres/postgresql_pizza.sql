/*  Create database */
createdb pizza
psql pizza



/*  Create users table */
CREATE TABLE users
(
    user_id        INTEGER PRIMARY KEY,
    email_address  VARCHAR(120) NOT NULL,
    first_name     VARCHAR(120) NOT NULL,
    last_name      VARCHAR(120) NOT NULL,
    phone_number   VARCHAR(10)  NOT NULL,
    address_line_1 VARCHAR(255) NOT NULL,
    address_line_2 VARCHAR(255) NOT NULL,
    city           VARCHAR(120) NOT NULL,
    state          CHAR(2)      NOT NULL,
    zip_code       VARCHAR(9)   NOT NULL,
    CONSTRAINT unique1 UNIQUE (email_address)
);

/*  load script users table */
\i /app/postgres/lectures/tempUsers.sql






/*  Create inventory table */
CREATE TABLE inventory
(
    inventory_id  INTEGER PRIMARY KEY,
    name          VARCHAR(120) NOT NULL,
    description   VARCHAR(255) NOT NULL,
    quantity      INTEGER      NOT NULL
);

/*  load script inventory table */
\i /app/postgres/lectures/tempIngredients.sql





/*  Create recipes table */
CREATE TABLE recipes
(
    recipe_id            INTEGER PRIMARY KEY,
    name                 VARCHAR(120) NOT NULL,
    description          VARCHAR(255) NOT NULL,
    cooking_instructions VARCHAR(255) NOT NULL
);

/*  load script inventory table */
\i /app/postgres/lectures/tempRecipes.sql







/*  Create inv_rec table */
CREATE TABLE inv_rec
(
    recipe_id    INTEGER REFERENCES recipes (recipe_id),
    inventory_id INTEGER REFERENCES inventory (inventory_id),
    inv_quantity INTEGER NOT NULL
);

/*  load script inv_rec table */
\i /app/postgres/lectures/tempInvRec.sql







/*  Create orders table */
CREATE TABLE orders
(
    order_id   INTEGER PRIMARY KEY,
    user_id    INTEGER   REFERENCES users (user_id),
    recipe_id  INTEGER   REFERENCES recipes (recipe_id),
    order_time TIMESTAMP NOT NULL
);

/*  load script orders table */
\i /app/postgres/lectures/tempOrders.sql

































































