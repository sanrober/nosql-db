CREATE OR REPLACE FUNCTION add_order(
    o_id INTEGER,
    u_id INTEGER,
    r_id INTEGER)
    RETURNS BOOLEAN AS $$
DECLARE
    order_time TIMESTAMP;
    new_inv INTEGER;
    inv_id INTEGER;
    inv_q INTEGER;
    current_quantity INTEGER;
    did_insert boolean := true;
    temp_row record;
BEGIN
    order_time := clock_timestamp();


    FOR temp_row IN
        SELECT *
        FROM inv_rec
        WHERE inv_rec.recipe_id = r_id
        LOOP
            inv_id := temp_row.inventory_id;
            inv_q := temp_row.inv_quantity;

            SELECT quantity INTO current_quantity
            FROM inventory
            WHERE inventory.inventory_id=inv_id;

            IF current_quantity < inv_q THEN
                RAISE NOTICE 'We do not have enough ingredients!';
                did_insert := false;
                EXIT;
            ELSE
                new_inv := current_quantity - inv_q;

                UPDATE inventory
                SET quantity = new_inv
                WHERE inventory_id=inv_id;
            END IF;
        END LOOP;

    IF did_insert = true THEN
        INSERT INTO orders (order_id, user_id, recipe_id, order_time)
        VALUES (o_id, u_id, r_id, order_time);
    ELSE
        RAISE NOTICE 'We do not have enough ingredients!';
    END IF;

    RETURN did_insert;
END;
$$ LANGUAGE plpgsql;


