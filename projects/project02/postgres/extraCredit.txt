1) How many doctors are treating doctors?

- To get list of all doctors treating doctors, do the following query:

select doctor_name, patient_name from doc_pat where patient_name Like 'Dr%';


- To get a list of distinct doctors treating doctors do the following query:

select distinct on (doctor_name)
doctor_name from doc_pat
where patient_name like 'Dr%';

- Finally to get a count of doctors who are treating other doctors, do the following query:

select count(distinct doctor_name) from doc_pat where patient_name like 'Dr%';
 count
-------
    47



2) What's the count of how many patients have each kind of illness?

select illness_name, count(patient_name)
from ill_pat
group by illness_name;

 illness_name | count
--------------+-------
 Illness928   |    15
 Illness553   |    18
 Illness149   |    13
 Illness248   |    14
 Illness345   |    19
... etc



3) What's the doctor with the most patients?

select doctor_name, count(patient_name)
from doc_pat
group by doctor_name
Order by count desc;

 doctor_name | count
-------------+-------
 Dr House72  |   284
 Dr House49  |   282
 Dr House28  |   282
 Dr House70  |   277
 Dr House13  |   277
 Dr House21  |   274


- Dr House72 has the most patients with 284 unique patients.

- We can grab just Dr House72 with the following query:

SELECT doctor_name
FROM doc_pat
GROUP BY doctor_name
HAVING COUNT(patient_name)=
    (SELECT MAX(patientcount) FROM
        (SELECT doctor_name,COUNT(patient_name) AS patientcount
         FROM doc_pat
         GROUP BY doctor_name) t1);

 doctor_name
-------------
 Dr House72



4) Which doctor is treating the largest number of unique illnesses?

- Combine doc_pat and ill_pat tables,to see what illnesses are treated by which doctor

select d.doctor_name, i.illness_name
from doc_pat d, ill_pat i
where d.patient_name=i.patient_name;


- Want to group by doctor name and count distinct illnesses

select d.doctor_name, count( distinct i.illness_name)
from doc_pat d, ill_pat i
where d.patient_name=i.patient_name
group by d.doctor_name
Order by count desc;

 doctor_name | count
-------------+-------
 Dr House28  |   363
 Dr House36  |   363
 Dr House21  |   353
 Dr House49  |   345
 Dr House99  |   341
 Dr House63  |   340


- Looks like Dr House28 and Dr House36 are both treating 363 distinct illnesses.





5) What illness is being treated with the largest number of unique treatments?

- Combine ill_pat and treat_pat tables,to see what illnesses are being treated

select i.illness_name, t.treatment_name
from ill_pat i, treat_pat t
where t.patient_name=i.patient_name;

- Want to group by illness name and count distinct treatments

select i.illness_name, count(distinct t.treatment_name)
from ill_pat i, treat_pat t
where t.patient_name=i.patient_name
group by i.illness_name
Order by count desc;

 illness_name | count
--------------+-------
 Illness533   |    73
 Illness43    |    70
 Illness44    |    68
 Illness336   |    66
 Illness169   |    66

 - Looks like Illness553 is being treated with 73 distinct treatments.





















